#!/usr/bin/env python3
import sys
import argparse
import os
import base64
import tty
import termios


def put(esc):
    if 'TMUX' in os.environ or 'screen' in os.environ['TERM']:
        esc = '\033Ptmux;\033' + esc + '\033\\'
    sys.stderr.write(esc)
    sys.stderr.flush()


def copy(cb, buf, max=74994):
    # Ignore trailing \r and \n
    if buf[-1] == 0x0a or buf[-1] == 0x0d:
        buf = buf[:-1]
    b = base64.b64encode(buf[:max]).decode()

    if 'kitty' in os.environ['TERM']:
        # Adapted from ojroques/vim-oscyank
        # Kitty versions below 0.22.0 require the clipboard to be flushed
        # before accepting a new string.
        # https://sw.kovidgoyal.net/kitty/changelog/#id33
        put('\033]52;' + cb + ';!\a')

    put('\033]52;' + cb + ';' + b + '\a')


def paste(cb):
    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)

    try:
        tty.setraw(sys.stdin.fileno())
        put('\033]52;' + cb + ';?\a')
        dat = ''
        while True:
            c = sys.stdin.read(1)
            dat += c
            if ord(c) == 7:
                break

        assert dat[:7] == '\033]52;' + cb + ';'

        return base64.b64decode(dat[7:-1].encode())
    finally:
        termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='OSC52 clipboard')

    mapping = {
        'clipboard': 'c',
        'clip': 'c',
        'c': 'c',
        'primary': 's',
        'p': 's',
        's': 's'
    }

    direction = parser.add_mutually_exclusive_group()
    direction.add_argument(
        '-i', '-in',
        action='store_true', default=True,
        help='read text into selection from standard input or files'
    )
    direction.add_argument(
        '-o', '-out',
        action='store_false',
        help='print the selection to standard out '
             '(generally for piping to a file or program)'
    )

    parser.add_argument(
        '-s', '-selection', '-sel',
        choices=mapping.keys(),
        default='primary',
        help='specify which X selection to use, options are '
             '"primary" to use XA_PRIMARY (default) or "clip'
             'board" for XA_CLIPBOARD'
    )

    parser.add_argument('file', nargs='?')

    args = parser.parse_args()

    cb = mapping[args.s]

    if args.o:
        if args.file:
            fp = open(args.file, 'rb')
        else:
            fp = sys.stdin.buffer

        copy(cb, fp.read())
    else:
        if args.file:
            fp = open(args.file, 'wb')
        else:
            fp = sys.stdout.buffer
        fp.write(paste(cb))

        if not args.file:
            fp.write(b'\n')
