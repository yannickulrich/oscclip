" vim-oscclip
" Author: Yannick Ulrich

if exists('g:loaded_oscclip') || &compatible
    finish
endif

let g:loaded_oscclip = 1

let g:oscclip_oscclip_cmd = get(g:, 'oscclip_oscclip_cmd', expand('<sfile>:p:h') . '/oscclip')
if !filereadable(g:oscclip_oscclip_cmd)
    let s:url = "https://gitlab.com/yannickulrich/oscclip/-/jobs/artifacts/master/raw/oscclip?job=build"

    if confirm("Do you want me to download a pre-build oscclip executable?", "&Yes\n&No") == 1
        call system("curl -L " . s:url . " -o " . g:oscclip_oscclip_cmd)
        call system("chmod +x " . g:oscclip_oscclip_cmd)
    else
        finish
    endif
endif

function! s:raw_echo(str)
    if filewritable('/dev/fd/2')
        call writefile([a:str], '/dev/fd/2', 'b')
    else
        exec("silent! !echo " . shellescape(a:str))
        redraw!
    endif
endfunction

function! s:parseReg(reg)
    if a:reg == "*"
        return "primary"
    elseif a:reg == "+"
        return "clipboard"
    else
        throw "Register needs to be either * or +"
    endif
endfunction

function! OSCYankVisual(...) range
    if a:0 == 1
        let l:cb="*"
    else
        let l:cb = a:2
    endif
    let l:rmode = a:1

    let [line_start, column_start] = getpos("'<")[1:2]
    let [line_end, column_end] = getpos("'>")[1:2]

    let lines = getline(line_start, line_end)
    if len(lines) > 0
        let lines[-1] = lines[-1][: column_end - (&selection == 'inclusive' ? 1 : 2)]
        let lines[0] = lines[0][column_start - 1:]

        call s:raw_echo(system(g:oscclip_oscclip_cmd . " -sel " . s:parseReg(l:cb), join(lines, "\n")))
    endif

    if l:rmode == 1
        normal! gv
    else
        execute "normal! `<"
    endif
endfunction

function! OSCPut(...)
    if a:0 == 0
        let l:cb="*"
    else
        let l:cb = a:1
    endif

    let l:tempname = tempname()
    let srr_save = &l:shellredir
    setl shellredir=>%s

    exe "silent! !" . g:oscclip_oscclip_cmd . " -o -sel " . s:parseReg(l:cb) . "> " . l:tempname
    let ans = readfile(l:tempname)
    let @o = join(ans,"\n")
    norm! "op

    let &l:shellredir = srr_save
    call delete(l:tempname)
    redraw!
endfunction

command! -range -nargs=? OSCYank <line1>,<line2>call OSCYankVisual(0, <f-args>)
command! -nargs=? OSCPut :call OSCPut(<f-args>)

if (len($DISPLAY) == 0) || exists('$TMUX')
    vnoremap <silent> <LeftRelease> :<C-U>call OSCYankVisual(1, "*")<CR>
    noremap <silent> <MiddleMouse> :<C-U>call OSCPut("*")<CR>
endif
