#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <termios.h>
#include <unistd.h>
#include "base64.h"

#define MAX_COPY_SIZE 74994
#define MAX_PASTE_SIZE 10000


struct termios orig_termios;
void restore_termios()
{
    tcsetattr(0, TCSANOW, &orig_termios);
}

void enter_paste_mode()
{
    // This saves the curernt termios configuration, disables ECHO and
    // ICANON, and adds an exit handler to restore termios.

    tcgetattr(0, &orig_termios);
    atexit(restore_termios);

    struct termios new = orig_termios;
    new.c_lflag &= ~ICANON;  // disable buffered IO
    new.c_lflag &= ~ECHO;    // disable ECHO

    tcsetattr(0, TCSANOW, &new);
}

uint8_t using_tmux()
{
    if(getenv("TMUX"))
        return 1;
    if(strstr("screen", "TERM"))
        return 1;
    return 0;
}


void copy(char cb, char *dat)
{
    size_t len;
    unsigned char *b64;

    // Ignore trailing \r and \n
    while(1)
    {
        len = strlen(dat);
        if( (dat[len-1] == '\n') || (dat[len-1] == '\r') )
            dat[len-1] = 0;
        else
            break;
    }

    b64 = base64_encode((unsigned char*)dat, strlen(dat), &len);

    if(using_tmux())
        fprintf(stderr, "\033Ptmux;\033");

    if(strstr("kitty", "TERM"))
        fprintf(stderr, "\033]52;%c;!\a", cb);

    fprintf(stderr, "\033]52;%c;%s\a", cb, b64);
    free(b64);

    if(using_tmux())
        fprintf(stderr, "\033\\");
    fflush(stderr);
}

char *check_input(char *c, char cb)
{
    if( *(c++) != '\033') return NULL;
    if( *(c++) != ']') return NULL;
    if( *(c++) != '5') return NULL;
    if( *(c++) != '2') return NULL;
    if( *(c++) != ';') return NULL;

    if( *c == cb )
    {
        ++c;
        if( *(c++) != ';') return NULL;
        return c;
    }
    else if ( *(c++) == ';' )
        return c;
    else
        return NULL;
}

char* paste(char cb, size_t *out_len)
{
    char buf[MAX_PASTE_SIZE];
    char *c;
    size_t len;
    uint8_t tmux = using_tmux();


    // Disable echo and buffered reading
    enter_paste_mode();

    // Emit signal
    if(tmux)
        fprintf(stderr, "\033Ptmux;\033");
    fprintf(stderr, "\033]52;%c;?\a", cb);
    if(tmux)
        fprintf(stderr, "\033\\");
    fflush(stderr);
    if(tmux)
    {
        sleep(0.1);
        fprintf(stderr, "\033]52;%c;?\a", cb);
    }

    // Read answer
    c = &buf[0]; len = 0;
    while(fread(c, 1, 1, stdin) == 1 && *c != 7)
    {
        c++; len++;
    }

    // Restore terminal
    restore_termios();

    // Check format
    if ((c = check_input(&buf[0], cb)))
    {
        // Looks good
        len -= c - &buf[0] - 1;
    }
    else
    {
        fprintf(stderr, "error: input mangled\n");
        return NULL;
    }

    return (char*)base64_decode((unsigned char*) c, len, out_len);
}


void do_copy(char cb, FILE *fp)
{
    char buf[MAX_COPY_SIZE];
    fread(buf, MAX_COPY_SIZE+1, 1, fp);

    copy(cb, buf);
}

void do_paste(char cb, FILE *fp)
{
    size_t len;
    char *buf = paste(cb, &len);
    fwrite(buf, len, 1, fp);
    free(buf);
}

#ifndef STANDALONE

int main(int argc, char *argv[])
{
    int i;
    char cb;
    enum mode{copy,paste} mode = copy;
    FILE *fp = NULL;

    i = 1;
    cb = 's';
    while(i < argc)
    {
        if(argv[i][0] == '-')
        {
            switch(argv[i][1])
            {
                // -s[election] <name>
                case 's':
                    cb = argv[++i][0];
                    // Primary -> Selection
                    if(cb == 'p') cb = 's';
                    break;

                // -o [<name>]
                case 'o':
                    mode = paste;
                    break;

                // -i [<name>]
                case 'i':
                    mode = copy;
                    break;

                case 0:
                    if(mode == copy)
                        fp = stdin;
                    else if(mode == paste)
                        fp = stdout;
            }
        }
        else
        {
            if(mode == copy)
                fp = fopen(argv[i], "rb");
            else if(mode == paste)
                fp = fopen(argv[i], "wb");
        }

        i++;
    }

    if(!fp){
        if(mode == copy)
            fp = stdin;
        else if(mode == paste)
            fp = stdout;
    }

    if(mode == copy)
        do_copy(cb, fp);
    if(mode == paste)
        do_paste(cb, fp);
    return 0;
}

#endif
