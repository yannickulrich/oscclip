# oscclip

`oscclip` is a small tool to access the system clipboards from
anywhere using the [ANSI OSC52 sequence](https://invisible-island.net/xterm/ctlseqs/ctlseqs.html#h4-Operating-System-Commands:OSC-Ps;Pt-ST:Ps-=-5-2.101B).

Since `oscclip` is not using
[`xclip`](https://github.com/astrand/xclip) or
[`xsel`](https://github.com/kfish/xsel) it is completely
location-independent and can be used over multiple SSH connections
without X forwarding (assuming your terminal supports OSC52).

`oscclip` is written in C even though a python implementation is also
available.

## Compilation
Just run
```console
$ gcc -o oscclip oscclip.c
```
Alternatively, download the executable (64bit, Linux) [here](https://gitlab.com/yannickulrich/oscclip/-/jobs/artifacts/master/raw/oscclip?job=build).

## Usage

`oscclip` can be used in the command line similar to `xclip`. Here are
some examples
```console
$ oscclip file_name           # Copies the content of file_name into primary
$ oscclip -s clip file_name   # Copies the content of file_name into the
                              # main clipboard
$ echo "stuff" | oscclip      # Copies stuff into primary
$ oscclip -o                  # Prints content of primary
$ oscclip -o | sort | uniq    # Prints duplicate free content of
                              # primary
```

### Using in `tmux`
`oscclip` can be used together with
[tmux-yank](https://github.com/tmux-plugins/tmux-yank) in `tmux`. To
recover select-to-copy and middle-click-paste, add the following to
your `tmux.conf`
```python
# ~/.tmux.conf

set -g @yank_selection 'primary'
set -g @yank_with_mouse on
set -g @override_copy_command "~/.tmux/oscclip/oscclip.py 2> #{pane_tty}"

bind -n MouseDown2Pane run "tmux set-buffer -b primary_selection \"$(~/.tmux/oscclip/oscclip.py -o < #{pane_tty} 2> #{pane_tty})\" ; tmux paste-buffer -b primary_selection; tmux delete-buffer -b primary_selection"

run '~/.tmux/plugins/tpm/tpm'
```

### Using in `vim`
To use `oscclip` in `vim`, source `oscclip.vim`. If you do not already
have the executable, `vim` will attempt to download it from the CI.
Otherwise, point the plugin to where you have it
```vim
let g:oscclip_oscclip_cmd = "/path/to/oscclip"
```

## Support for terminals
(copied from [vim-oscyank](https://github.com/ojroques/vim-oscyank))

| Terminal | OSC52 support |
|----------|:-------------:|
| [Alacritty](https://github.com/alacritty/alacritty) | **yes** |
| [GNOME Terminal](https://github.com/GNOME/gnome-terminal) (and other VTE-based terminals) | [not yet](https://bugzilla.gnome.org/show_bug.cgi?id=795774) |
| [hterm (Chromebook)](https://chromium.googlesource.com/apps/libapps/+/master/README.md) | [**yes**](https://chromium.googlesource.com/apps/libapps/+/master/nassh/doc/FAQ.md#Is-OSC-52-aka-clipboard-operations_supported) |
| [iTerm2](https://iterm2.com/) | **yes** |
| [kitty](https://github.com/kovidgoyal/kitty) | **yes** |
| [screen](https://www.gnu.org/software/screen/) | **yes** |
| [tmux](https://github.com/tmux/tmux) | **yes** |
| [Windows Terminal](https://github.com/microsoft/terminal) | **yes** |
| [rxvt](http://rxvt.sourceforge.net/) | **yes** (to be confirmed) |
| [urxvt](http://software.schmorp.de/pkg/rxvt-unicode.html) | **yes** (with a script, see [here](https://github.com/ojroques/vim-oscyank/issues/4)) |


## References
 * [Copying to clipboard from tmux and Vim using OSC 52](https://sunaku.github.io/tmux-yank-osc52.html)
 * [vim-oscyank](https://github.com/ojroques/vim-oscyank)
 * [base64 in C](http://web.mit.edu/freebsd/head/contrib/wpa/)
